import click
import json

import plotly.graph_objects as go

from registry.registry_factory import get_registry, get_point_in_time_registry
from registry.registry_factory import list_groups as registy_list_groups
from player.constants import TIED

from rich.console import Console
from rich.table import Table
from datetime import datetime


def _to_dict(player):
    return {"name": player.name, "rating": player.rating}


@click.group()
def cli():
    pass


@click.command()
@click.option("--group", required=True, type=str)
@click.argument("names", nargs=2)
@click.option("--winner", required=True, type=str)
@click.option("--description", required=False, type=str)
def match(group, names, winner, description):
    try:
        if winner not in names and winner != TIED:
            raise ValueError(f"Illegal winner: {winner}")

        get_registry(group).match(names[0], names[1], winner, description=description)
        click.echo(
            json.dumps(
                {
                    "success": True,
                    "updated_players": [
                        _to_dict(get_registry(group).players[names[0]]),
                        _to_dict(get_registry(group).players[names[1]]),
                    ],
                },
                indent=2,
            )
        )

    except Exception as e:
        click.echo(json.dumps({"success": False, "error": str(e)}, indent=2))


@click.command()
@click.option("--group", required=True, type=str)
@click.argument("names", nargs=2)
def odds_of_winning(group, names):
    try:
        first_player_name = names[0]
        second_player_name = names[1]

        if (
            first_player_name not in get_registry(group).players
            or second_player_name not in get_registry(group).players
        ):
            raise ValueError("Unknown player(s)")

        first_player = get_registry(group).players[first_player_name]
        second_player = get_registry(group).players[second_player_name]

        click.echo(
            json.dumps(
                {
                    "success": True,
                    "odds_of_winning": {
                        first_player_name: first_player.get_expected_score(
                            second_player
                        ),
                        second_player_name: second_player.get_expected_score(
                            first_player
                        ),
                    },
                },
                indent=2,
            )
        )

    except Exception as e:
        click.echo(json.dumps({"success": False, "error": str(e)}, indent=2))


@click.command()
@click.option("--group", required=True, type=str)
def plot(group):
    if group not in registy_list_groups():
        click.echo(json.dumps({"success": False, "error": "Unknown group"}, indent=2))
        return

    point_in_time_registry = get_point_in_time_registry(group)

    player_name_to_timeseries = {}

    while point_in_time_registry.has_match_to_replay():
        current_replay_time = point_in_time_registry.current_replay_time
        current_player_names = point_in_time_registry.current_player_names
        point_in_time_registry.advance()

        for player_name in current_player_names:
            player = point_in_time_registry.players[player_name]

            if not player.name in player_name_to_timeseries:
                player_name_to_timeseries[player.name] = {"dates": [], "ratings": []}

            player_name_to_timeseries[player.name]["dates"].append(current_replay_time)
            player_name_to_timeseries[player.name]["ratings"].append(player.rating)

    # Show where everyone falls right now.
    for player_name in player_name_to_timeseries:
        player_name_to_timeseries[player_name]["dates"].append(datetime.now())
        player_name_to_timeseries[player_name]["ratings"].append(point_in_time_registry.players[player_name].rating)

    fig = go.Figure()
    fig.update_layout(
        title="Elo ratings over time",
        xaxis_title="Date",
        yaxis_title="Rating",
        legend_title="Players",
    )

    for player_name in player_name_to_timeseries:
        fig.add_trace(
            go.Scatter(
                x=player_name_to_timeseries[player_name]["dates"],
                y=player_name_to_timeseries[player_name]["ratings"],
                mode="lines+markers",
                name=player_name,
            )
        )

    fig.show()


@click.command()
@click.option("--group", required=True, type=str)
def table(group):
    if group not in registy_list_groups():
        click.echo(json.dumps({"success": False, "error": "Unknown group"}, indent=2))
        return

    table = Table()
    table.add_column("", no_wrap=True)

    for player in get_registry(group).ordered_players:
        table.add_column(f"{player.name} ({int(player.rating)})")

    for player in get_registry(group).ordered_players:
        expected_odds = [
            str(round(player.get_expected_score(other), 3))
            for other in get_registry(group).ordered_players
        ]

        table.add_row(
            f"{player.name} ({int(player.rating)})",
            *expected_odds,
        )

    console = Console()
    console.print(table, justify="left")


@click.command()
@click.option("--group", required=True, type=str)
def list_players(group):
    if group not in registy_list_groups():
        click.echo(json.dumps({"success": False, "error": "Unknown group"}, indent=2))
        return

    click.echo(
        json.dumps(
            {"players": [_to_dict(x) for x in get_registry(group).ordered_players]},
            indent=2,
        )
    )


@click.command()
@click.option("--group", required=True, type=str)
@click.argument("player", required=True)
def describe_player(group, player):
    if group not in registy_list_groups():
        click.echo(json.dumps({"success": False, "error": "Unknown group"}, indent=2))
        return

    players = get_registry(group).players

    if player not in players:
        click.echo(json.dumps({"success": False, "error": "Unknown player"}, indent=2))
        return

    click.echo(
        json.dumps(
            {
                "name": players[player].name,
                "rating": players[player].rating,
                "num_matches": players[player].num_matches,
                "highest_rating": players[player].highest_rating,
                "longest_winning_streak": players[player].longest_winning_streak,
                "current_winning_streak": players[player].current_winning_streak,
            },
            indent=2,
        )
    )


@click.command()
def list_groups():
    click.echo(json.dumps({"groups": registy_list_groups()}, indent=2))


cli.add_command(match)
cli.add_command(odds_of_winning)
cli.add_command(list_groups)
cli.add_command(list_players)
cli.add_command(describe_player)
cli.add_command(table)
cli.add_command(plot)
