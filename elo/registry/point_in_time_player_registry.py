PLAYERS = "players"
WINNER = "WINNER"
DATE = "date"


class PointInTimePlayerRegistry:
    def __init__(self, registry):
        self.registry = registry
        self.matches_to_replay = []

    def match(self, player_1_name, player_2_name, winner, date):
        self.matches_to_replay.append(
            {PLAYERS: [player_1_name, player_2_name], WINNER: winner, DATE: date}
        )

    def has_match_to_replay(self):
        return len(self.matches_to_replay) > 0

    def advance(self):
        match_to_replay = self.matches_to_replay.pop(0)
        self.registry.match(
            match_to_replay[PLAYERS][0],
            match_to_replay[PLAYERS][1],
            match_to_replay[WINNER],
            match_to_replay[DATE],
        )

    @property
    def players(self):
        return self.registry.players

    @property
    def ordered_players(self):
        return self.registry.ordered_players

    @property
    def current_player_names(self):
        return self.matches_to_replay[0][PLAYERS]

    @property
    def current_replay_time(self):
        if not self.has_match_to_replay():
            raise ValueError()

        return self.matches_to_replay[0]["date"]
