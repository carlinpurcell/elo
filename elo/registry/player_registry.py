from player.player import Player

DEFAULT_RATING = 1200


class PlayerRegistry:
    def __init__(self):
        self.players = {}

    def _populate_if_missing(self, player_name):
        if not player_name in self.players:
            self.players[player_name] = Player(player_name, DEFAULT_RATING)

    def match(self, player_1_name, player_2_name, winner, date):
        self._populate_if_missing(player_1_name)
        self._populate_if_missing(player_2_name)

        self.players[player_1_name].match(self.players[player_2_name], winner)

    @property
    def ordered_players(self):
        return sorted(
            self.players.values(),
            key=lambda x: x.rating,
            reverse=True,
        )
