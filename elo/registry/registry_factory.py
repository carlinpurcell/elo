import os

from registry.file_persisting_player_registry import FilePersistingPlayerRegistry
from registry.player_registry import PlayerRegistry
from registry.point_in_time_player_registry import PointInTimePlayerRegistry

REGISTRY_RESULTS_BASE = os.path.expanduser("~/.elo/")
REGISTRY_EXTENSION = ".nd"


def get_registry(group):
    return FilePersistingPlayerRegistry(
        REGISTRY_RESULTS_BASE + group + REGISTRY_EXTENSION, PlayerRegistry()
    )


def get_point_in_time_registry(group):
    return FilePersistingPlayerRegistry(
        REGISTRY_RESULTS_BASE + group + REGISTRY_EXTENSION,
        PointInTimePlayerRegistry(PlayerRegistry()),
    ).registry


def list_groups():
    return [x.strip(REGISTRY_EXTENSION) for x in os.listdir(REGISTRY_RESULTS_BASE)]
