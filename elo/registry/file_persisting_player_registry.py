import json
import os

from registry.player_registry import PlayerRegistry
from player.constants import TIED
from datetime import datetime

PLAYERS = "players"
WINNER = "winner"
DESCRIPTION = "description"
DATETIME = "datetime"


class FilePersistingPlayerRegistry:
    def __init__(self, results_filepath, registry):
        self.results_filepath = results_filepath
        self.registry = registry
        self._init()

    def _init(self):
        try:
            os.makedirs(os.path.dirname(self.results_filepath), exist_ok=True)

            self._populate()
        except FileNotFoundError:
            # This is the first time we're seeing this group name
            # There are no previous results to populate.
            return

    def _populate(self):
        with open(self.results_filepath, "r") as f:
            for line in f:
                result_dict = json.loads(line)

                self.registry.match(
                    result_dict[PLAYERS][0],
                    result_dict[PLAYERS][1],
                    result_dict[WINNER],
                    datetime.fromtimestamp(float(result_dict[DATETIME])),
                )

    def match(self, player_1_name, player_2_name, winner, description=None):
        current_datetime = datetime.now()

        self._write_result(
            json.dumps(
                {
                    PLAYERS: [player_1_name, player_2_name],
                    WINNER: winner,
                    DESCRIPTION: description,
                    DATETIME: datetime.timestamp(current_datetime),
                }
            )
            + "\n"
        )

        self.registry.match(player_1_name, player_2_name, winner, current_datetime)

    def _write_result(self, result, file_open_mode="a"):
        try:
            with open(self.results_filepath, file_open_mode) as f:
                f.write(result)
        except FileNotFoundError:
            if file_open_mode == "a":
                self._write_result(result, file_open_mode="w")

    @property
    def players(self):
        return self.registry.players

    @property
    def ordered_players(self):
        return self.registry.ordered_players
