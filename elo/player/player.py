from player.constants import TIED


class Player:
    def __init__(self, name, rating):
        self.name = name
        self.rating = rating
        self.num_matches = 0
        self.highest_rating = rating
        self.longest_winning_streak = 0
        self.current_winning_streak = 0

    def get_expected_score(self, other):
        return 1.0 / (1 + 10 ** ((other.rating - self.rating) / 400.0))

    def _adjust_rating_and_stats(self, expected_score, actual_score, k=32):
        new_rating = self.rating + k * (actual_score - expected_score)

        self.highest_rating = max(new_rating, self.highest_rating)
        self.rating = new_rating

        self.num_matches += 1
        self.current_winning_streak = (
            self.current_winning_streak + 1 if actual_score == 1 else 0
        )
        self.longest_winning_streak = max(
            self.longest_winning_streak, self.current_winning_streak
        )

    def match(self, other, result):
        scores = {
            self.name: {self.name: 1, other.name: 0},
            other.name: {self.name: 0, other.name: 1},
            TIED: {self.name: 0.5, other.name: 0.5},
        }

        self._adjust_rating_and_stats(
            self.get_expected_score(other), scores[result][self.name]
        )
        other._adjust_rating_and_stats(
            other.get_expected_score(self), scores[result][other.name]
        )
